package com.arccus.pickupmylaundry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.model.DrycleanList;
import com.arccus.pickupmylaundry.model.DrycleanName;

import java.util.ArrayList;

/*
 * Created by KCS on 25-Dec-18.
 * Email : info.kaprat@gmail.com
 */
public class DrycleanNameAdpter  extends RecyclerView.Adapter<DrycleanNameAdpter.MyViewHolder> {

    private Context context;
    private ArrayList<DrycleanName> drycleanNameArrayList = new ArrayList<>();

    private LinearLayoutManager drycleanLayoutManager;

    public DrycleanNameAdpter(Context context, ArrayList<DrycleanName> drycleanNameArrayList){

        this.context = context;
        this.drycleanNameArrayList = drycleanNameArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dryclean_name,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DrycleanName drycleanName = drycleanNameArrayList.get(position);

        holder.tv_dryclean_name.setText(drycleanName.getDryclean_name());

        ArrayList<DrycleanList> drycleanArrayList = drycleanName.getDrycleanListArray();

        drycleanLayoutManager = new LinearLayoutManager(context);
        drycleanLayoutManager.scrollToPosition(0);
        holder.rv_dryclean_detail.setLayoutManager(drycleanLayoutManager);
        holder.rv_dryclean_detail.setHasFixedSize(true);

        DrycleanAdapter drycleanAdapter = new DrycleanAdapter(context, drycleanArrayList);
        holder.rv_dryclean_detail.setAdapter(drycleanAdapter);
    }

    @Override
    public int getItemCount() {
        return drycleanNameArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dryclean_name;
        private RecyclerView rv_dryclean_detail;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_dryclean_name = (TextView) itemView.findViewById(R.id.tv_dryclean_name);
            rv_dryclean_detail = (RecyclerView) itemView.findViewById(R.id.rv_dryclean_detail);

        }
    }
}
