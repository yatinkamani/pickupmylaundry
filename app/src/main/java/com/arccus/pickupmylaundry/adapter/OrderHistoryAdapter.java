package com.arccus.pickupmylaundry.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.activity.OrderDetailsActivity;
import com.arccus.pickupmylaundry.model.OrderHistoryData;

import java.util.ArrayList;

/*
 * Created by KCS on 22-Nov-18.
 * Email : info.kaprat@gmail.com
 */
public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OrderHistoryData> orderHistoryArrayList = new ArrayList<>();

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistoryData> orderHistoryArrayList){
        this.context = context;
        this.orderHistoryArrayList = orderHistoryArrayList;
    }

    @NonNull
    @Override
    public OrderHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_row,parent,false);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHistoryAdapter.MyViewHolder holder, int position) {

        OrderHistoryData orderHistoryData = orderHistoryArrayList.get(position);

        final String orderNO = orderHistoryData.getOrder_no();
        holder.tvOrderNo.setText(orderNO);
        // SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        holder.tvOrderDate.setText(orderHistoryData.getOrder_date());
        holder.tvOrderDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("ORDER_NO", orderNO);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderHistoryArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrderNo, tvOrderDate, tvOrderDetail;
        public MyViewHolder(View itemView) {
            super(itemView);

            tvOrderNo = (TextView) itemView.findViewById(R.id.tvOrderNo);
            tvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);
            tvOrderDetail = (TextView) itemView.findViewById(R.id.tvOrderDetail);
        }
    }
}
