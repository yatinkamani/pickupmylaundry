package com.arccus.pickupmylaundry.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.adapter.OrderedClothAdapter;
import com.arccus.pickupmylaundry.model.OrderedClothList;
import com.arccus.pickupmylaundry.utils.API;
import com.arccus.pickupmylaundry.utils.APIResponse;
import com.arccus.pickupmylaundry.utils.Helper;
import com.arccus.pickupmylaundry.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OrderDetailsActivity extends AppCompatActivity {

    private ImageView ivBack;
    private RecyclerView rvClothList;
    private LinearLayoutManager orderedclothLayoutmanager;
    private OrderedClothAdapter orderedClothAdapter;
    private OrderedClothList orderedClothList;
    private ArrayList<OrderedClothList> orderedClothArrayList;
    TextView tv_customer_name, tv_order_no, tv_order_date, tv_customer_address,
            tv_delivery_date, tv_service_type, tv_total_cloth, tv_order_status;

    private String orderNo = "";
    private String total_cloth ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        orderNo = getIntent().getStringExtra("ORDER_NO");

        ivBack = (ImageView) findViewById(R.id.ivBack);

        tv_customer_name = (TextView) findViewById(R.id.tv_customer_name);
        tv_order_no = (TextView) findViewById(R.id.tv_order_no);
        tv_order_date = (TextView) findViewById(R.id.tv_order_date);
        tv_customer_address = (TextView) findViewById(R.id.tv_customer_address);
        tv_delivery_date = (TextView) findViewById(R.id.tv_delivery_date);
        tv_service_type = (TextView) findViewById(R.id.tv_service_type);
        tv_total_cloth = (TextView) findViewById(R.id.tv_total_cloth);
        tv_order_status = (TextView) findViewById(R.id.tv_order_status);


        rvClothList = (RecyclerView)findViewById(R.id.rvClothList);
        orderedclothLayoutmanager = new LinearLayoutManager(OrderDetailsActivity.this);
        orderedclothLayoutmanager.scrollToPosition(0);
        // overlayLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvClothList.setLayoutManager(orderedclothLayoutmanager);
        rvClothList.setHasFixedSize(true);

        if(!TextUtils.isEmpty(orderNo)){
            getOrderDetail();
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getOrderDetail(){

        if (Helper.isNetworkAvailable(OrderDetailsActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("ORDER DETAILS : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            orderedClothArrayList = new ArrayList<>();

                            JSONObject result = object.getJSONObject("result");

                            String user_name = result.getString("user_name");
                            String order_no = result.getString("order_no");
                            String order_date = result.getString("order_date");
                            String address = result.getString("address");
                            String delivery_date = result.getString("delivery_date");
                            total_cloth = result.getString("total_clothes");
                            String order_status = result.getString("order_status");

                            tv_customer_name.setText(user_name);
                            tv_order_no.setText( order_no);
                            tv_order_date.setText(order_date);
                            tv_customer_address.setText(address);
                            tv_delivery_date.setText("Delivery Date: " + delivery_date);
                            tv_service_type.setText("Types of Service: "+ "Wash & Iron");
                            tv_total_cloth.setText(total_cloth);

                            if(order_status.equalsIgnoreCase("1")){
                                tv_delivery_date.setVisibility(View.VISIBLE);
                                tv_order_status.setText("Delivered");

                            }else {
                                tv_delivery_date.setVisibility(View.GONE);
                                tv_order_status.setText("Pending");
                            }

                            JSONArray type_of_service = result.getJSONArray("type_of_service");

                            for (int i = 0; i < type_of_service.length(); i++) {

                                JSONObject cldetail = type_of_service.getJSONObject(i);

                                String no_of_garments = cldetail.getString("no_of_garments");
                                String garment_name = cldetail.getString("garment_name");

                                orderedClothList = new OrderedClothList();
                                orderedClothList.setCloth_name(garment_name);
                                orderedClothList.setCloth_quantity(no_of_garments);

                                orderedClothArrayList.add(orderedClothList);
                            }

                           /* orderedClothAdapter = new OrderedClothAdapter( OrderDetailsActivity.this, orderedClothArrayList);

                            rvClothList.setAdapter(orderedClothAdapter);

                            int resId = R.anim.layout_animation_from_bottom;
                            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(OrderDetailsActivity.this, resId);
                            rvClothList.setLayoutAnimation(animation);*/

                        } else {
                            Helper.showToast(OrderDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(OrderDetailsActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("order_no", orderNo);

            API api = new API(OrderDetailsActivity.this, apiResponse);
            api.execute(1, Services.GET_ORDER_DETAIL, params, true);

        } else {
            Helper.showToast(OrderDetailsActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
