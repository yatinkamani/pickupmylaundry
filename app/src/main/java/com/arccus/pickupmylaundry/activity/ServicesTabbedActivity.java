package com.arccus.pickupmylaundry.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.fragments.BedFragment;
import com.arccus.pickupmylaundry.fragments.HoseHoldFragment;
import com.arccus.pickupmylaundry.fragments.ManFragment;
import com.arccus.pickupmylaundry.fragments.WomenFragment;
import com.arccus.pickupmylaundry.model.ClothDataList;

import java.util.ArrayList;
import java.util.List;

public class ServicesTabbedActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ClothDataList clothDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_tabbed);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
       // viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ManFragment(), "Men's");
        adapter.addFragment(new WomenFragment(), "Women's");
        adapter.addFragment(new BedFragment(), "Bedding");
        adapter.addFragment(new HoseHoldFragment(), "Household");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
