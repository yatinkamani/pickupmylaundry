package com.arccus.pickupmylaundry.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.adapter.PackageAdapter;
import com.arccus.pickupmylaundry.adapter.PackageNameAdapter;
import com.arccus.pickupmylaundry.model.PackageList;
import com.arccus.pickupmylaundry.model.PackageName;
import com.arccus.pickupmylaundry.utils.API;
import com.arccus.pickupmylaundry.utils.APIResponse;
import com.arccus.pickupmylaundry.utils.Helper;
import com.arccus.pickupmylaundry.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WashIronActivity extends AppCompatActivity {

    private ImageView ivBack;

    private RecyclerView rvPackage;
    private LinearLayoutManager packageLayoutmanager;
    private PackageNameAdapter packageAdapter;
    private PackageList packageList;
    private PackageName packageName;

    private ArrayList<PackageName> packageNameArrayList;
    private ArrayList<PackageList> packageArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_iron);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvPackage = (RecyclerView) findViewById(R.id.rvPackage);
        packageLayoutmanager = new LinearLayoutManager(WashIronActivity.this);
        packageLayoutmanager.scrollToPosition(0);
        //  packageLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPackage.setLayoutManager(packageLayoutmanager);
        rvPackage.setHasFixedSize(true);

        getPackageList();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void getPackageList() {

        packageArrayList = new ArrayList<>();
        packageNameArrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(WashIronActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("Package List : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject result = object.getJSONObject("result");

                            JSONArray package_plan_array = result.getJSONArray("package_plan");
                            for (int j = 0; j < package_plan_array.length(); j++) {

                                JSONObject package_plan_object = package_plan_array.getJSONObject(j);
                                String package_plan_id = package_plan_object.getString("package_plan_id");
                                String package_id = package_plan_object.getString("package_id");
                                String package_plan_person = package_plan_object.getString("package_plan_person");
                                String package_plan_price = package_plan_object.getString("package_plan_price");
                                String package_plan_discount_price = package_plan_object.getString("package_plan_discount_price");
                                String package_plan_final_price = package_plan_object.getString("package_plan_final_price");
                                String package_plan_cloth_limit = package_plan_object.getString("package_plan_cloth_limit");

                                packageList = new PackageList();
                                packageList.setPackage_plan_id(package_plan_id);
                                packageList.setPackage_id(package_id);
                                packageList.setPackage_person(package_plan_person);
                                packageList.setPackage_price(package_plan_price);
                                packageList.setDiscount_price(package_plan_discount_price);
                                packageList.setFinal_price(package_plan_final_price);
                                packageList.setCloth_limit(package_plan_cloth_limit);
                                packageArrayList.add(packageList);
                            }

                            JSONArray package_name_array = result.getJSONArray("package_name");

                            for (int i = 0; i < package_name_array.length(); i++) {

                                JSONObject package_name_object = package_name_array.getJSONObject(i);
                                String package_id = package_name_object.getString("package_id");
                                String package_name = package_name_object.getString("package_name");

                                packageName = new PackageName();
                                packageName.setPackage_id(package_id);
                                packageName.setPackage_name(package_name);

                                ArrayList<PackageList> packageArrayList1 = new ArrayList<>();

                                for (int k = 0; k < packageArrayList.size(); k++) {
                                    PackageList packageList1 = packageArrayList.get(k);

                                    if (package_id.equalsIgnoreCase(packageList1.getPackage_id())) {
                                        packageArrayList1.add(packageList1);
                                    }
                                }
                                packageName.setPackageListArray(packageArrayList1);
                                packageNameArrayList.add(packageName);
                            }

                            packageAdapter = new PackageNameAdapter(WashIronActivity.this, packageNameArrayList);
                            rvPackage.setAdapter(packageAdapter);

                        } else {
                            Helper.showToast(WashIronActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(WashIronActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(WashIronActivity.this, apiResponse);
            api.execute(1, Services.PACKAGE_LIST, params, true);
        } else {
            Helper.showToast(WashIronActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
