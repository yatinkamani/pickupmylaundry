package com.arccus.pickupmylaundry.activity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.adapter.DrycleanAdapter;
import com.arccus.pickupmylaundry.adapter.DrycleanNameAdpter;
import com.arccus.pickupmylaundry.model.DrycleanList;
import com.arccus.pickupmylaundry.model.DrycleanName;
import com.arccus.pickupmylaundry.utils.API;
import com.arccus.pickupmylaundry.utils.APIResponse;
import com.arccus.pickupmylaundry.utils.Helper;
import com.arccus.pickupmylaundry.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DryCleanPricingActivity extends AppCompatActivity {

    private ImageView ivBack;
    private RecyclerView rvDryClean;
    private LinearLayoutManager drycleanLayoutmanager;
    private DrycleanNameAdpter drycleanAdapter;
    private DrycleanList drycleanList;
    private DrycleanName drycleanName;

    private ArrayList<DrycleanName> drycleanNameArrayList;
    private ArrayList<DrycleanList> drycleanArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dry_clean_pricing);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ivBack = (ImageView) findViewById(R.id.ivBack);

        rvDryClean = (RecyclerView) findViewById(R.id.rvDryClean);
        drycleanLayoutmanager = new LinearLayoutManager(DryCleanPricingActivity.this);
        drycleanLayoutmanager.scrollToPosition(0);
        //  packageLayoutmanager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvDryClean.setLayoutManager(drycleanLayoutmanager);
        rvDryClean.setHasFixedSize(true);

        getDrycleanPrice();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void getDrycleanPrice(){

        drycleanArrayList = new ArrayList<>();
        drycleanNameArrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(DryCleanPricingActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    Helper.showLog("DRY CLEAN LIST : " + response);
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONObject result = object.getJSONObject("result");

                            JSONArray dry_cleaning_array = result.getJSONArray("dry_cleaning");
                            for (int j = 0; j < dry_cleaning_array.length(); j++) {

                                JSONObject dry_clean_object = dry_cleaning_array.getJSONObject(j);
                                String cloth_id  = dry_clean_object.getString("dry_id");
                                String garment_category_id = dry_clean_object.getString("garment_category_id");
                                String cloth_name = dry_clean_object.getString("garment_name");
                                String cloth_price = dry_clean_object.getString("dryclean_price");

                                drycleanList = new DrycleanList();
                                drycleanList.setCloth_id(cloth_id);
                                drycleanList.setGarment_category_id(garment_category_id);
                                drycleanList.setCloth_name(cloth_name);
                                drycleanList.setCloth_price(cloth_price);
                                drycleanArrayList.add(drycleanList);
                            }

                            JSONArray garment_category_array = result.getJSONArray("garment_group");

                            for (int i = 0; i < garment_category_array.length(); i++) {

                                JSONObject dry_clean_object = garment_category_array.getJSONObject(i);
                                String dryclean_category_id = dry_clean_object.getString("garment_category_id");
                                String dryclean_category_name = dry_clean_object.getString("garment_category_name");

                                drycleanName = new DrycleanName();
                                drycleanName.setDryclean_id(dryclean_category_id);
                                drycleanName.setDryclean_name(dryclean_category_name);

                                ArrayList<DrycleanList> drycleanArrayList1  = new ArrayList<>();

                                for(int k = 0; k<drycleanArrayList.size(); k++){
                                    DrycleanList drycleanList1 = drycleanArrayList.get(k);

                                    if(dryclean_category_id.equalsIgnoreCase(drycleanList1.getGarment_category_id())){
                                        drycleanArrayList1.add(drycleanList1);
                                    }

                                }

                                drycleanName.setDrycleanListArray(drycleanArrayList1);

                                drycleanNameArrayList.add(drycleanName);
                            }

                            drycleanAdapter = new DrycleanNameAdpter(DryCleanPricingActivity.this, drycleanNameArrayList);
                            rvDryClean.setAdapter(drycleanAdapter);

                        } else {
                            Helper.showToast(DryCleanPricingActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(DryCleanPricingActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("email", "");

            API api = new API(DryCleanPricingActivity.this, apiResponse);
            api.execute(1, Services.DRY_CLEANING, params, true);
        } else {
            Helper.showToast(DryCleanPricingActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
