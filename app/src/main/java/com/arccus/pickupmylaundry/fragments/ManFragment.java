package com.arccus.pickupmylaundry.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.pickupmylaundry.R;

public class ManFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public ManFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("MAN","True");
        return inflater.inflate(R.layout.fragment_man, container, false);
    }

    public interface OnFragmentInteractionListener {
        
    }
}
