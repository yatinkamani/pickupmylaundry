package com.arccus.pickupmylaundry.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.arccus.pickupmylaundry.R;
import com.arccus.pickupmylaundry.activity.DryCleanPricingActivity;
import com.arccus.pickupmylaundry.activity.WashIronActivity;


public class HomeFragment extends Fragment {

    public LinearLayout llDryClean, llWashIron;
    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        llDryClean = (LinearLayout) view.findViewById(R.id.llDryClean);
        llWashIron = (LinearLayout) view.findViewById(R.id.llWashIron);

        llDryClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DryCleanPricingActivity.class);
                startActivity(intent);
            }
        });

        llWashIron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent washIron_intent = new Intent(getActivity(), WashIronActivity.class);
                startActivity(washIron_intent);
            }
        });


        return view;
    }


    public interface OnFragmentInteractionListener {

    }
}
