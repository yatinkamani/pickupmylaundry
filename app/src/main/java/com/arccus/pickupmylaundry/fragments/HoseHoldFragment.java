package com.arccus.pickupmylaundry.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.pickupmylaundry.R;

public class HoseHoldFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public HoseHoldFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("HOSEHOLD","True");
        return inflater.inflate(R.layout.fragment_hose_hold, container, false);
    }

    public interface OnFragmentInteractionListener {

    }
}
